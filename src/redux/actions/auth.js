import { AUTHENTICATE, DEAUTHENTICATE } from '../actionTypes';
import Api from '../config/Api';

const authenticateAction = (user) => {
  return {
    type: AUTHENTICATE,
    payload: user,
  };
};


const deAuthenticateAction = () => {
  return {
    type: DEAUTHENTICATE,
  };
};


export const login = (formData) => (dispatch) => {
  const data = new FormData();
  data.append('username', formData.username);
  data.append('password', formData.password);

  return fetch(Api.getAuthApi(), {
    method: 'POST',
    body: data,
  })
    .then((response) => response.json())
    .then((result) => {
      dispatch(deAuthenticateAction());

      const payload = {
        user: result.user,
        error: result.error,
      };

      return dispatch(authenticateAction(payload));
    })
    .catch(() => dispatch(deAuthenticateAction()));
};

export const logout = () => async (dispatch) => {
  dispatch(deAuthenticateAction());

  return Promise.resolve();
};
