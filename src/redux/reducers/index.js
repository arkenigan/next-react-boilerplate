import { combineReducers } from 'redux';
import { AUTHENTICATE, DEAUTHENTICATE } from '../actionTypes';

const authReducer = (state = { user: null, error: null }, action) => {
  switch (action.type) {
    case AUTHENTICATE:
      return {
        ...state,
        user: action.payload.user,
        error: action.payload.error,
      };
    case DEAUTHENTICATE:
      return {
        ...state,
        user: null,
        error: null,
      };
    default:
      return state;
  }
};

export default combineReducers({
  auth: authReducer,
});
