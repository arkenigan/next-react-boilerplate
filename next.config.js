const withCss = require("@zeit/next-css");
const withSass = require("@zeit/next-sass");
const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === 'true',
});


const nextConfig = {
  experimental: {
    css: true,
  },
  webpack: (config, { dev }) => {
    return config;
  }
};

module.exports = withBundleAnalyzer(
  withSass(
    withCss(nextConfig),
  ),
);

